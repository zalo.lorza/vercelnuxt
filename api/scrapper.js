const got = require('got')
const htmlparser = require('node-html-parser')

const parser = (raw) => {

  if(raw.offers && !Array.isArray(raw.offers)){
    raw.offers = [raw.offers]
  }
  

  const data = {
    title: raw.title ?? raw['og:title'] ?? raw['twitter:title'] ?? null,
    name: raw.name ?? raw.title ?? raw['og:title'] ?? raw['twitter:title'] ?? null,
    description: raw.description ?? raw['og:description'] ?? raw['twitter:description'] ?? null,
    price: raw.price ?? raw['og:price:amount'] ?? raw.offers?.[0]?.price ?? ( raw.offers?.[0]?.lowPrice ? (raw.offers?.[0]?.lowPrice === raw.offers?.[0]?.highPrice ? raw.offers?.[0]?.lowPrice : `${raw.offers?.[0]?.lowPrice} - ${raw.offers?.[0]?.highPrice}`) : null),
    currency: raw.currency ?? raw.price ?? raw.priceCurrency ?? raw['og:price:currency'] ?? raw.offers?.[0]?.currency ?? raw.offers?.[0]?.priceCurrency ?? null,
    images: [
      raw.image ?? raw['og:image:secure_url'] ?? raw['og:image'] ?? null
    ],
    brand: raw.brand?.name ?? raw.brand ?? null,
    offers: raw.offers ?? null
  }


  if (data.offers && Array.isArray(data.offers)) {
    data.offers.forEach(offer => {
      data.images.push(offer?.image ?? null)
    })
  }

  let images = []
  data.images.forEach(image => {
    if (image && Array.isArray(image)) {
      images = [...images, ...image]
    } else {
      images.push(image)
    }
  })

  images = new Set(images)
  data.images = [...images]

  return {
    data,
    raw
  }
}

module.exports = async (req, res) => {
  try {
    const { body } = await got(req.query.url)
    const document = htmlparser.parse(body)

    let data = {};
    data['title'] = document.querySelector("title").innerText
    
    document.querySelectorAll("head > meta").forEach((tag) => {
      data[`${tag.getAttribute('property') ?? tag.getAttribute('name')}`] = tag.content
    });
    data["application/ld+json"] = []
    document.querySelectorAll('script[type="application/ld+json"]').forEach((jsonScript) => {
      const content = jsonScript.innerText.trim().replace(/(\r\n|\n|\r)/gm, "")
      try {
        data = { ...data, ...JSON.parse(content) }
      } catch (error) {
        data["application/json"].push(jcontent)
      }
    })
    data["application/json"] = []
    document.querySelectorAll('script[type="application/json"]').forEach((jsonScript) => {
      const content = jsonScript.innerText.trim().replace(/(\r\n|\n|\r)/gm, "")
      try {
        data = { ...data, ...JSON.parse(content) }
      } catch (error) {
        data["application/json"].push(jcontent)
      }
    })


    res.json(parser(data))
  } catch (error) {
    res.json({ error: error.message, query: req.query })
  }
}